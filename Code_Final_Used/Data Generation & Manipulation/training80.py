__author__ = "Can Ozbek Arnav"

import pandas as pd
import numpy as np
import pylab
import matplotlib.pyplot as plt
from sklearn.metrics import confusion_matrix

import sys
sys.path.append("/Users/ahmetcanozbek/Desktop/EE660/660Project/Code_Final_Used/functions")
import ml_aux_functions as ml_aux
import crop_rock


#PREPROCESSING
#Read the files
df_train = pd.read_pickle("/Users/ahmetcanozbek/Desktop/660Stuff/msd_train.pkl") # 80%
print "DEBUG: file read."
#Get rid of the rows that have missing values (nan) and UNCAT
df_train = df_train[ df_train["Genre"] != "UNCAT" ]
df_train =  df_train.dropna()
print "DEBUG: dropna() done."
y = df_train["Genre"]
X = df_train.drop(["Genre", "Track ID", "Year"], axis=1)
#PREPROCESSING DONE

#Split the 80% of data to Training and Validation Data
#Training 70%
#Validation 30%
from sklearn.cross_validation import train_test_split
X_train, X_validation, y_train, y_validation = train_test_split(X, y, train_size=0.7)





print "Plotting the histogram"
ml_aux.plot_histogram(ml_aux.getUniqueCount(df_train["Genre"]))
plt.show()


#Adaboost
from sklearn.ensemble import AdaBoostClassifier
adaboost_model = AdaBoostClassifier(n_estimators=50)
#Train
adaboost_model.fit(X,y)
print "DEBUG: fit done."
#Predict
y_predicted = adaboost_model.predict(X)
print "DEBUG: predict done."

print "Error rate: ", ml_aux.get_error_rate(y,y_predicted)

print ml_aux.plot_confusion_matrix(y,y_predicted, "AdaBoost")
plt.show()



